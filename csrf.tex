\documentclass[aspectratio=1610]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{minted}
\usepackage{graphicx}
\usepackage{listings}

\usetheme{LSE}

\title{CSRF: Cross-Site Request Forgery}

\author[Louis Feuvrier, Bruno Pujos, Pierre Rovis]{
    \texttt{Louis Feuvrier} \textemdash{} manny@lse.epita.fr\\
    \texttt{Bruno Pujos} \textemdash{} bruno@lse.epita.fr\\
    \texttt{Pierre Rovis} \textemdash{} pierre.rovis@lse.epita.fr
}
\institute{\textbf{LSE}, EPITA Systems Lab. \\ \url{http://lse.epita.fr/}}

\date{}

\begin{document}

\begin{frame}[plain]
    \titlepage
\end{frame}

\begin{frame}{Table of Contents}
    \tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}{Introduction}
    \begin{itemize}
        \item{Coerce another user into performing actions}
    \end{itemize}
\end{frame}

\begin{frame}{Examples}
    \begin{itemize}
        \item{PHPBB admin removing messages}
        \item{Promoting admin rights on malicious account}
        \item{Trigger bank transfers to malicious bank account}
    \end{itemize}
\end{frame}

\begin{frame}{History}
    \begin{itemize}
        \item{Brought up in 1988 by Norm Hardy}
        \item{Discovery in 2000 that ZOPE is affected}
        \item{CSRF term coined in 2001 by Peter Wakins on Bugtraq ML}
    \end{itemize}
\end{frame}

\begin{frame}{What are the possibilities?}
    \begin{itemize}
        \item{Trigger every functionality allowed by the website}
        \item{Every basic thing user can do can be triggered via CSRF}
    \end{itemize}
\end{frame}

\begin{frame}{The concept}
    \includegraphics[scale=0.44]{content/csrf/explanation.pdf}
\end{frame}

\section{GET method}

\begin{frame}{GET method}
    \begin{center}
        \inputminted{html}{content/csrf/get.html}
    \end{center}
\end{frame}

\begin{frame}{The original request}
    GET http://bank.fr/transfer.php?acc=Bob\&amnt=100 HTTP/1.1
    \begin{center}
        \Huge{vs.}
    \end{center}
    \vspace{0.25cm}
    GET http://bank.fr/transfer.php?acc=Mallory\&amnt=100000 HTTP/1.1
\end{frame}

\begin{frame}{The basic approach}
    \inputminted{html}{content/csrf/href.html}

    \begin{itemize}
        \item{Target address can be seen when hovering the link}
        \item{Alice will realize what's happened once the transfer is done}
    \end{itemize}
\end{frame}

\begin{frame}{Better ways}
    \inputminted{html}{content/csrf/image.html}

    \begin{itemize}
        \item{Browser will indicate that it's not able to render the image}
        \item{Alice automatically submits the GET request trying to retrieve
            the image}
    \end{itemize}
\end{frame}

\begin{frame}{Demonstration}
    \begin{center} \Huge{Demonstration} \end{center}
\end{frame}

\begin{frame}{Exercise: GET}
    \begin{center} \Huge{Exercise: GET} \end{center}

    \begin{itemize}
        \item{http://louis.feuvrier.org/supinternet/csrf101.zip}
        \item{http://louis.feuvrier.org/supinternet/csrf101.tar.gz}
    \end{itemize}
\end{frame}

\section{POST method}

\begin{frame}{POST method}
    \begin{center}
        \inputminted{html}{content/csrf/post.html}
    \end{center}
\end{frame}

\begin{frame}
    \begin{center} \Huge{\$\_REQUEST} \end{center}
\end{frame}

\begin{frame}{Perl's CGI.PM library}
    \begin{itemize}
        \item{Doesn't care wether args are POST or GET}
        \item{POST requests can be passed as GET}
        \item{Affects all libraries depending on it}
    \end{itemize}
\end{frame}

\begin{frame}{Auto sending of POST forms}
    \inputminted{html}{content/csrf/jspost.html}

    \begin{itemize}
        \item{Problem: user will be redirected to confirmation page if plain}
        \item{Solution: hide page in an iframe}
    \end{itemize}
\end{frame}

\begin{frame}{Plain javascript forgery of POST forms}
    \inputminted{javascript}{content/xss/autosend.js}
\end{frame}

\begin{frame}{Am I vulnerable?}
    \begin{itemize}
        \item{Browse through proxy like Paros}
        \item{Record requests made while browsing}
        \item{Perform same actions}
    \end{itemize}
\end{frame}

\begin{frame}{Exercise: POST}
    \begin{center} \Huge{Exercise: POST} \end{center}

    \begin{itemize}
        \item{http://louis.feuvrier.org/supinternet/csrf102.zip}
        \item{http://louis.feuvrier.org/supinternet/csrf102.tar.gz}
    \end{itemize}
\end{frame}

\section{Prevention}

\begin{frame}{Only acception POST requests}
    \begin{center} \Huge{NO} \end{center}
\end{frame}

\begin{frame}{Using a secret cookie}
    \begin{center} \Huge{NO} \end{center}
\end{frame}

\begin{frame}{Client/User prevention}
    \begin{itemize}
        \item{Do not allow your browser to save usernames/passwords}
        \item{Do not use the same browser to access sensitive applications}
        \item{Log off immediately after using a Web application}
        \item{Plugins such as No-Script}
    \end{itemize}
\end{frame}

\begin{frame}{Synchronizer token}
    \inputminted{html}{content/csrf/token.html}
\end{frame}

\begin{frame}{Exercise: TOKEN}
    \begin{center} \Huge{Exercise: TOKEN} \end{center}

    \begin{itemize}
        \item{http://louis.feuvrier.org/supinternet/csrf103.zip}
        \item{http://louis.feuvrier.org/supinternet/csrf103.tar.gz}
    \end{itemize}
\end{frame}

\section{Conclusion}

\begin{frame}{Past exploitations}
    \begin{itemize}
        \item{GMail: allowing to steal user's contact list}
        \item{Netflix: allow changing name/address, queue movies, ...}
        \item{NYTimes: display user's hidden email addresss}
        \item{YouTube: countless CRSFs everywhere...}
        \item{Ruby on Rails: allowed bypass of csrf protection}
        \item{...}
    \end{itemize}
\end{frame}

\begin{frame}{Frameworks builtin protections}
    \begin{itemize}
        \item{RoR: http://guides.rubyonrails.org/security.html\#cross-site-request-forgery-csrf}
        \item{Django: https://docs.djangoproject.com/en/dev/ref/contrib/csrf/}
        \item{Symfony: http://symfony.com/doc/current/book/forms.html\#forms-csrf}
    \end{itemize}
\end{frame}

\begin{frame}{Conclusion}
    \begin{itemize}
        \item{CSRF: abuses websites' trust for users}
        \item{XSS: abuses users' trust for websites}
    \end{itemize}
\end{frame}

\begin{frame}{Questions?}
    \begin{center}
        \includegraphics[scale=0.44]{content/question-mark}
    \end{center}
\end{frame}

\end{document}
