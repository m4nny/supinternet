<?php require 'vendor/autoload.php';

session_start();

function check($sid, $name) {
    $link = mysqli_connect('localhost', 'root', '', 'csrf');
    $result = mysqli_query($link, "SELECT * FROM users WHERE sid='$sid'");
    if (!$result)
        printf("Error: %s\n", mysqli_error($link));
    $row = mysqli_fetch_array($result);
    mysqli_close($link);
    return $row['name'] === $name;
}

function form($action, $method) {
?>
    <form action='<?php echo $action; ?>' method='<?php echo $method; ?>'>
        <input type='text' name='name' />
        <input type='password' name='pwd' />
        <input type='submit' value='ok' />
    </form>
<?php
}

$app = new \Slim\Slim();

$app->get('/login', function () {
    if (isset($_SESSION['name']) && check(session_id(), $_SESSION['name'])) {
        echo 'already logged in!<br/>';
        echo 'goto <a href="/main">main</a>';
    }
    else {
        echo 'login form (name, password)<br/>';
        form('/login', 'post');
    }
});

$app->post('/login', function() {
    if (isset($_SESSION['name']) && check(session_id(), $_SESSION['name'])) {
        echo 'already logged in!<br/>';
        echo 'goto <a href="/main">main</a>';
    }
    else {
        /* Connect to database and fetch password matching the given name */
        $link = mysqli_connect('localhost', 'root', '', 'csrf');
        $name = mysqli_real_escape_string($link, htmlspecialchars($_POST['name']));
        $result = mysqli_query($link, "SELECT pwd FROM users WHERE name='$name'");
        if (!$result)
            printf("Error: %s\n", mysqli_error($link));
        $row = mysqli_fetch_array($result);

        if ($row['pwd'] === $_POST['pwd']) {
            $sid = session_id();
            $result = mysqli_query($link, "UPDATE users SET sid='$sid' WHERE name='$name'");
            if (!$result)
                printf("Error: %s\n", mysqli_error($link));
            $_SESSION['name'] = $name;

            echo 'login successful<br/>';
            echo 'goto <a href="/main">main</a>';
        }
        else {
            ?>
            <p>
                login failed!<br />
                <a href='/login'>retry!</a>
            </p>
            <?php
        }

        mysqli_close($link);
    }
});

$app->get('/logout', function() {
    $link = mysqli_connect('localhost', 'root', '', 'csrf');
    $name = mysqli_real_escape_string($link, htmlspecialchars($_SESSION['name']));
    $result = mysqli_query($link, "UPDATE users SET sid=NULL WHERE name='$name'");
    if (!$result)
        printf("Error: %s\n", mysqli_error($link));
    mysqli_close($link);

    session_destroy();
    echo '<p><a href="/login">login page</a>';
});

$app->get('/register_form', function() {
    if (isset($_SESSION['name']) && check(session_id(), $_SESSION['name'])) {
        echo 'register form (name, password): <br/>';
        form('/register_treatment', 'get');
    }
    else {
        echo 'you do not have these rights';
    }
});

$app->get('/register_treatment', function() {
    if (isset($_SESSION['name']) && check(session_id(), $_SESSION['name'])) {
        $link = mysqli_connect('localhost', 'root', '', 'csrf');
        $name = mysqli_real_escape_string($link, htmlspecialchars($_GET['name']));
        $pwd = mysqli_real_escape_string($link, htmlspecialchars($_GET['pwd']));

        $result = mysqli_query($link, "SELECT * FROM users WHERE name='$name'");
        if (!$result)
            printf("Error: %s\n", mysqli_error($link));
        $row = mysqli_fetch_array($result);

        if ($row === NULL) {
            $result = mysqli_query($link, "INSERT INTO users (name, pwd)
                VALUES('$name', '$pwd')");
            if (!$result)
                printf("Error: %s\n", mysqli_error($link));
            echo 'username added <br/>';
            echo 'goto <a href="/main">main</a>';
        }
        else {
            echo 'username already in use';
        }

        mysqli_close($link);
    }
});

$app->get('/main', function() {
?>
    <p>
        <a href="/register_form">register somebody</a><br />
        <a href="/logout">logout</a><br/>
    </p>
    <p>
        also, eve's spaghettis are real, real bad. bleh.
    </p>
<?php
});

$app->run();

?>
