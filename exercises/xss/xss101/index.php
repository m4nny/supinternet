<?php require 'vendor/autoload.php';

$app = new \Slim\Slim();

$app->get('/', function() {
?>
    <p><a href='/post'>leave a message!</a></p>
    <hr />
<?php
    $link = mysqli_connect('localhost', 'root', '', 'xss');
    $result = mysqli_query($link, "SELECT * FROM guestbook");

    $row = mysqli_fetch_array($result);
    while ($row) {
?>
        <p><?php echo $row['pseudo']; ?></p>
        <p><?php echo $row['message']; ?></p>
        <hr />
<?php
        $row = mysqli_fetch_array($result);
    }

    mysqli_close($link);
});

$app->get('/post', function() {
?>
    <p>Type your pseudonyme and message!</p>
    <form action='/post' method='post'>
        <input type='text' name='pseudo' />
        <input type='textarea' name='message' />
        <input type='submit' value='ok' />
    </form>
<?php
});

$app->post('/post', function() {
    if (isset($_POST['pseudo']) && isset($_POST['message'])) {
        $pseudo = $_POST['pseudo'];
        $message = $_POST['message'];

        $link = mysqli_connect('localhost', 'root', '', 'xss');
        $result = mysqli_query($link,
            "INSERT INTO guestbook VALUES('$pseudo', '$message')");
        if (!$result)
            printf("Error: %s\n", mysqli_error($link));
        mysqli_close($link);

        echo 'Message added successfully';
        echo "go back to <a href='/'>messages</a>";
    }
    else {
        echo 'missing arguments to post entry';
    }
});

$app->run();

?>
