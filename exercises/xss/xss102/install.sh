#! /bin/sh

COMPOSER_URL='https://getcomposer.org/installer';
COMPOSER_CONTENT='{ "require": { "slim/slim": "2.*" } }';

curl -s $COMPOSER_URL | php;
echo $COMPOSER_CONTENT > composer.json;
php composer.phar install;
