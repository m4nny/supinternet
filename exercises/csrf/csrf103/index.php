<?php require 'vendor/autoload.php';

session_start();

function check($sid, $name) {
    $link = mysqli_connect('localhost', 'root', '', 'csrf');
    $result = mysqli_query($link, "SELECT * FROM users WHERE sid='$sid'");
    if (!$result)
        printf("Error: %s\n", mysqli_error($link));
    $row = mysqli_fetch_array($result);
    mysqli_close($link);
    return $row['name'] === $name;
}

function form($action, $method, $token) {
?>
    <form action='<?php echo $action; ?>' method='<?php echo $method; ?>'>
        <input type='hidden' name='token' value='<?php echo $token; ?>' />
        <input type='text' name='name' />
        <input type='password' name='pwd' />
        <input type='submit' value='ok' />
    </form>
<?php
}

$app = new \Slim\Slim();

$app->get('/login', function () {
    if (isset($_SESSION['name']) && check(session_id(), $_SESSION['name'])) {
        echo 'already logged in!<br/>';
        echo 'goto <a href="/main">main</a>';
    }
    else {
        echo 'login form (name, password)<br/>';
        $_SESSION['token'] = md5(uniqid(rand(), TRUE));
        form('/login', 'post', $_SESSION['token']);
    }
});

$app->post('/login', function() {
    if (isset($_SESSION['name']) && check(session_id(), $_SESSION['name'])) {
        echo 'already logged in!<br/>';
        echo 'goto <a href="/main">main</a>';
    }
    else {
        if (isset($_SESSION['token']) && $_SESSION['token'] === $_POST['token']) {
            /* Connect to database and fetch password matching the given name */
            $link = mysqli_connect('localhost', 'root', '', 'csrf');
            $name = mysqli_real_escape_string($link,
                htmlspecialchars($_POST['name']));
            $result = mysqli_query($link,
                "SELECT pwd FROM users WHERE name='$name'");
            if (!$result)
                printf("Error: %s\n", mysqli_error($link));
            $row = mysqli_fetch_array($result);

            if ($row['pwd'] === $_POST['pwd']) {
                $sid = session_id();
                $result = mysqli_query($link,
                    "UPDATE users SET sid='$sid' WHERE name='$name'");
                if (!$result)
                    printf("Error: %s\n", mysqli_error($link));
                $_SESSION['name'] = $name;

                echo 'login successful<br/>';
                echo 'goto <a href="/main">main</a>';
            }
            else {
                ?>
                <p>
                    login failed!<br />
                    <a href='/login'>retry!</a>
                </p>
                <?php
            }

            mysqli_close($link);
        }
        else {
            echo 'token invalid!';
        }
    }
});

$app->get('/logout', function() {
    $link = mysqli_connect('localhost', 'root', '', 'csrf');
    $name = mysqli_real_escape_string($link, htmlspecialchars($_SESSION['name']));
    $result = mysqli_query($link, "UPDATE users SET sid=NULL WHERE name='$name'");
    if (!$result)
        printf("Error: %s\n", mysqli_error($link));
    mysqli_close($link);

    session_destroy();
    echo '<p><a href="/login">login page</a>';
});

$app->get('/register_form', function() {
    if (isset($_SESSION['name']) && check(session_id(), $_SESSION['name'])) {
        $_SESSION['token'] = md5(uniqid(rand(), TRUE));
        echo 'register form (name, password): <br/>';
        form('/register_treatment', 'post');
    }
    else {
        echo 'you do not have these rights';
    }
});

$app->post('/register_treatment', function() {
    if (isset($_SESSION['name']) && check(session_id(), $_SESSION['name'])) {
        if (isset($_SESSION['token']) && $_SESSION['token'] === $_POST['token']) {
            $link = mysqli_connect('localhost', 'root', '', 'csrf');
            $name = mysqli_real_escape_string($link,
                htmlspecialchars($_POST['name']));
            $pwd = mysqli_real_escape_string($link,
                htmlspecialchars($_POST['pwd']));

            $result = mysqli_query($link,
                "SELECT * FROM users WHERE name='$name'");
            if (!$result)
                printf("Error: %s\n", mysqli_error($link));
            $row = mysqli_fetch_array($result);

            if ($row === NULL) {
                $result = mysqli_query($link, "INSERT INTO users (name, pwd)
                    VALUES('$name', '$pwd')");
                if (!$result)
                    printf("Error: %s\n", mysqli_error($link));
                echo 'username added <br/>';
                echo 'goto <a href="/main">main</a>';
            }
            else {
                echo 'username already in use';
            }

            mysqli_close($link);
        }
        else {
            echo 'token invalid!';
        }
    }
});

$app->get('/main', function() {
?>
    <p>
        <a href="/register_form">register somebody</a><br />
        <a href="/logout">logout</a><br/>
    </p>
    <p>
        also, eve's spaghettis are real, real bad. bleh.
    </p>
<?php
});

$app->run();

?>
