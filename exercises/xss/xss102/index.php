<?php require 'vendor/autoload.php';

function reload($addr) {
?>
    <a href='<?php echo $addr; ?>'>reload the current page</a>
<?php
}

$app = new \Slim\Slim();

$app->get('/', function() {
?>
    <h1>List of recipes</h1>

    <ul>
        <li><a href="/choucroute">Choucroute garnie</a></li>
        <li><a href="/cake">Chocolate cake</a></li>
        ... And more to come!
    </ul>
<?php

    reload('/');
});

$app->get('/choucroute', function() {
?>
    <h2>Choucroute Garnie</h2>

    <ul>
        <li>1/3 cup kosher salt, plus more for seasoning</li>
        <li>2 tablespoons light brown sugar</li>
        <li>3 pounds pork back ribs or baby back ribs, cut into 3 sections</li>
        <li>6 pounds sauerkraut (in plastic bags), drained</li>
        <li>1/4 cup duck or goose fat or peanut oil</li>
        <li>1 large onion, coarsely chopped</li>
        <li>4 large garlic cloves, coarsely chopped</li>
        <li>20 juniper berries</li>
        <li>3 large bay leaves</li>
        <li>1/2 teaspoon caraway seeds</li>
        <li>1 teaspoon freshly ground black pepper</li>
        <li>3 cups chicken stock</li>
        <li>1 1/2 cups Riesling or Pinot Gris</li>
        <li>2 pounds Polish kielbasa, skinned and cut into 2-inch pieces</li>
        <li>10 skinless hot dogs</li>
        <li>One 2-pound piece of boneless boiled ham (3 to 4 inches wide), sliced 1/4 inch thick</li>
        <li>2 pounds medium potatoes (about 10), peeled</li>
        <li>Assorted mustards, for serving</li>
    </ul>

<?php
    reload('/choucroute');
});

$app->get('/cake', function() {
?>
    <h2>Chocolate cake</h2>

    <ul>
        <li>225g/8oz plain flour</li>
        <li>350g/12½oz caster sugar</li>
        <li>85g/3oz cocoa powder</li>
        <li>1½ tsp baking powder</li>
        <li>1½ tsp bicarbonate of soda</li>
        <li>2 free-range eggs</li>
        <li>250ml/9fl oz milk</li>
        <li>125ml/4½fl oz vegetable oil</li>
        <li>2 tsp vanilla extract</li>
        <li>250ml/9fl oz boiling water</li>
    </ul>
<?php

    reload('/cake');
});

$app->notFound(function() {
    $uri = $_SERVER['REQUEST_URI'];

    /* beautify the string (remove leading slash to have a correct article name) */
    $uri = mb_substr($uri, 1, strlen($uri));
    $safe_uri = htmlspecialchars($uri);
?>
    <h2>404 page not found!</h2>

    <p>
        The article about <?php echo $uri; ?> has not been found, we
        are sorry for the inconvenience.
    </p>
<?php

    reload($uri);
});

$app->run();

?>
